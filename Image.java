import java.util.Arrays;

public class Image {
	private String imageType;
	private int width;
	private int height;
	private int maxiumumColorValue;
	private Pixel[][] imageArray;
	
	public Image(String imageString) {
		
		String cleanImageString = imageString.trim().replaceAll("#.*?\n","");
		
		// Parse out the image into tokens
		String[] imageTokens = cleanImageString.split("\\s+");
		
		// The first token is the image type: P3
		imageType = imageTokens[0];

		// The second token is the width as ASCII characters in decimal
		width = Integer.parseInt(imageTokens[1]);

		// The third token is the height as ASCII characters in decimal
		height = Integer.parseInt(imageTokens[2]);

		// The four token is the maxiumum color value as ASCII characters in decimal
		maxiumumColorValue = Integer.parseInt(imageTokens[3]);

		// The last tokens are sets of 3 numbers from to 0 - maximum color value (255 for this project) the width as ASCII characters in decimal
		imageArray = new Pixel[height][width];

		int row = 0;
		int column = 0;
		int tokenNumber = 4;
		while (tokenNumber < imageTokens.length) {
			if (column == width) {
				column = 0;
				row++;
				continue;
			}
			Pixel pixel = new Pixel(Integer.parseInt(imageTokens[tokenNumber]), Integer.parseInt(imageTokens[tokenNumber + 1]), Integer.parseInt(imageTokens[tokenNumber + 2]));
			imageArray[row][column] = pixel;
			tokenNumber += 3;
			column++;
		}
	}

	public void invert() {
		for (int i = 0; i<imageArray.length;i++) {
			for (int j = 0; j<imageArray[i].length;j++) {
				imageArray[i][j].invert(maxiumumColorValue);
			}
		}
	}

	public void grayscale() {
		for (int i = 0; i<imageArray.length;i++) {
			for (int j = 0; j<imageArray[i].length;j++) {
				imageArray[i][j].grayscale();
			}
		}	
	}

	public void emboss() {
		for (int i = imageArray.length - 1; i >= 0;i--) {
			for (int j = imageArray[i].length - 1; j>=0;j--) {
				if (i == 0 || j == 0) {
					imageArray[i][j].emboss();
					continue;
				}
				imageArray[i][j].emboss(imageArray[i-1][j-1]);
			}
		}	
	}

	public void motionBlur(int motionBlurLength) {
		Pixel[][] placeHolderImage = new Pixel[height][width];
	//	copyArray(imageArray, placeHolderImage);
		for (int i = 0; i<imageArray.length;i++) {
			for (int j = 0; j<imageArray[i].length;j++) {
				int redTotal = 0;
				int greenTotal = 0;
				int blueTotal = 0;
				int numberOfPixelsAveraged = 0;
				for (int k = j; k < j + motionBlurLength;k++) {
					if (k >= width) {
						continue;
					}

					redTotal += imageArray[i][k].getRedValue();
					greenTotal += imageArray[i][k].getGreenValue();
					blueTotal += imageArray[i][k].getBlueValue();
					numberOfPixelsAveraged++;
				}	
				imageArray[i][j].motionBlur(redTotal/numberOfPixelsAveraged, greenTotal/numberOfPixelsAveraged, blueTotal/numberOfPixelsAveraged);
			}
		}
	//	copyArray(placeHolderImage, imageArray);
	}
	
	private void copyArray(Pixel[][] duplicatedArray, Pixel[][] newArray) {
		for(int i=0; i<duplicatedArray.length; i++)
  			for(int j=0; j<duplicatedArray[i].length; j++)
    				newArray[i][j]=duplicatedArray[i][j];
	}

	public String toString() {
		StringBuilder imageString = new StringBuilder();
		imageString.append(imageType);
		imageString.append("\n");
		imageString.append(width);
		imageString.append(" ");
		imageString.append(height);
		imageString.append("\n");
		imageString.append(maxiumumColorValue);
		imageString.append("\n");
		for (Pixel[] array : imageArray) {
			for (Pixel pixel : array) {
				imageString.append(pixel.toString());
			}
		}
		return imageString.toString();
	}
}
