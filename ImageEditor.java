import java.io.*;
import java.util.*;

public class ImageEditor {

	public static void main(String[] args) {
		// Create instance of ImageEditor class		
		ImageEditor imageEditor = new ImageEditor();

		// If the command line arguments are incorrect, display usage message error and quit.
		if (!imageEditor.parseCommandLine(args)) {
			imageEditor.invalidArguments();
			System.exit(0);
		}

		// Open the image file to read
		String imageString = imageEditor.readFileIntoString(args[0]);
		
	//	System.out.println(imageString);

		// Create an instance of that image class
		Image image = new Image(imageString);

		// Modify that instance according to the applied filter
		if (args[2].equals("invert")) {
			image.invert();
		}
		else if (args[2].equals("invert")) {
			image.invert();
		}
		else if (args[2].equals("grayscale")) {
			image.grayscale();
		}
		else if (args[2].equals("emboss")) {
			image.emboss();
		}
		else if (args[2].equals("motionblur")) {
			image.motionBlur(Integer.parseInt(args[3]));
		}

		// Save the new image to the file location
		imageEditor.saveImageToFile(image, args[1]);
	}
	
	private String readFileIntoString(String fileLocation) {
		StringBuilder content = new StringBuilder();
		File file = new File(fileLocation);
		try (Scanner scan = new Scanner(file)) {
       			while (scan.hasNextLine()) {
			content.append("\n");
            		content.append(scan.nextLine());
        		}
		}
		catch (FileNotFoundException ex) {
			System.out.println("Could not find file: " + file);
			ex.printStackTrace();
		}
		catch (IllegalArgumentException ex) {
			System.out.println("Illegal argument: Fix your bug!");
			ex.printStackTrace();
		}
		catch (Exception ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		}
		return content.toString();
	}

	private void saveImageToFile(Image image, String fileLocation) {
		File file = new File(fileLocation);
		try (PrintWriter writer = new PrintWriter(file)) {
			writer.println(image.toString());
		}
		catch (FileNotFoundException ex) {
			System.out.println("Could not find file: " + file);
			ex.printStackTrace();
		}
		catch (IllegalArgumentException ex) {
			System.out.println("Illegal argument: Fix your bug!");
			ex.printStackTrace();
		}
		catch (Exception ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		}	
	}

	private boolean parseCommandLine(String[] args) {
		// If there are not enough arguments, they are not valid arguments 
		if (args.length < 3) {
			return false;
		}
		// If we should motion blur the picture but they don't provide a valid (or any) blurring length, we should return false
		else if (args[2].equals("motionblur")) {  
			if (args.length != 4) {
				return false;
			}
			try {
				int motionBlurLength = Integer.parseInt(args[3]);
				if (motionBlurLength <= 0) {
					return false;
				}
			}
			catch (NumberFormatException e) {
				return false;
			}
		}
		// The third argument must be a valid effect, if not, return false (and there should be only three arguments)
		else if (!args[2].equals("invert") && !args[2].equals("grayscale") && !args[2].equals("emboss") || args.length == 4) {
			return false;
		}
		return true;
	}

	private void invalidArguments() {
		System.out.println("Invalid arguments, please revise the command line arguments.");
		System.out.println("USAGE: java ImageEditor in-file out-file (grayscale|invert|emboss|motionblur motion-blur-length)");
	}
}
