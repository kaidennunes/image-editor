public class Pixel {
	private int red;
	private int green;
	private int blue;

	private int defaultEmbossValue = 128;

	public Pixel(int redValue, int greenValue, int blueValue) {
		red = redValue;
		green = greenValue;
		blue = blueValue;
	}

	public void invert(int maxiumumColorValue) {
		red = Math.abs(red - maxiumumColorValue);
		green = Math.abs(green - maxiumumColorValue);
		blue = Math.abs(blue - maxiumumColorValue);
	}

	public void grayscale() {
		int newColorValue = (red + green + blue) / 3;
		red = newColorValue;
		green = newColorValue;
		blue = newColorValue;
	}

	public void emboss() {
		red = defaultEmbossValue;
		green = defaultEmbossValue;
		blue = defaultEmbossValue;
	}

	public void emboss(Pixel pixel) {
		int redDifference = red - pixel.getRedValue();
		int greenDifference = green - pixel.getGreenValue();
		int blueDifference = blue - pixel.getBlueValue();
		
		int largestDifference = redDifference;
		if (Math.abs(greenDifference) > Math.abs(largestDifference)) {
			largestDifference = greenDifference;
		}
		
		if (Math.abs(blueDifference) > Math.abs(largestDifference)) {
			largestDifference = blueDifference;
		}
		
		int v = defaultEmbossValue + largestDifference;
		if (v < 0) {
			v = 0;
		}
		else if (v > 255) {
			v = 255;
		}
		red = v;
		green = v;
		blue = v;
	}

	public void motionBlur(int redValue, int greenValue, int blueValue) {
		red = redValue;
		green = greenValue;
		blue = blueValue;
	}

	public int getRedValue() {
		return red;
	}

	public int getGreenValue() {
		return green;
	}

	public int getBlueValue() {
		return blue;
	}

	public String toString() {
		return String.format("%d\n%d\n%d\n", red, green, blue);
	}
}
